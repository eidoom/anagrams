#!/usr/bin/env python3

import collections

if __name__ == "__main__":
    with open("/usr/share/dict/words", "r") as f:
        raw = f.read()

    words = raw.strip().split("\n")

    collector = collections.defaultdict(list)

    for word in words:
        normal_form = "".join(sorted(word.lower()))
        collector[normal_form].append(word)

    all_anagrams = [anagrams for anagrams in collector.values() if len(anagrams) > 1 and len(anagrams[0]) > 3]
    all_anagrams.sort(key=lambda x: len(x))

    for anagrams in all_anagrams:
        print(anagrams)
